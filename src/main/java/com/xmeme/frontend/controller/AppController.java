package com.xmeme.frontend.controller;

import com.xmeme.frontend.dtos.PostDto;
import com.xmeme.frontend.exceptions.BackendServerError;
import com.xmeme.frontend.services.AppService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AppController {

    public static final String BASE_URL = "/memes";

    @Autowired
    private AppService appService;

    @GetMapping(BASE_URL)
    public String home(Model model) throws BackendServerError{
        
        List<PostDto> posts = appService.getPosts();
        model.addAttribute("Posts", posts);

        return "home";
    }
    
}
