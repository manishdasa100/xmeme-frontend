package com.xmeme.frontend.exceptions;

import org.springframework.http.HttpStatus;

public class ApplicationError extends Exception{
    
    public static final String DEFAULT_MSG = "Internal Server error!";

    private HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;

    public ApplicationError() {
        super(DEFAULT_MSG);
    }

    public ApplicationError(HttpStatus httpStatus) {
        super(DEFAULT_MSG);
        this.httpStatus = httpStatus;
    }

    public ApplicationError(String msg) {
        super(msg);
    }

    public ApplicationError(String msg, HttpStatus httpStatus) {
        super(msg);
        this.httpStatus = httpStatus;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }
}
