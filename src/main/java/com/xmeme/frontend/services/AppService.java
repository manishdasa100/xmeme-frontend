package com.xmeme.frontend.services;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.xmeme.frontend.dtos.PostDto;
import com.xmeme.frontend.exceptions.BackendServerError;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

@Service
public class AppService {
    
    private static RestTemplate restTemplate = new RestTemplate();

    public List<PostDto> getPosts() throws BackendServerError{

        String url = "https://api-memescape.herokuapp.com/memes";

        try{
            PostDto[] posts = restTemplate.getForObject(url, PostDto[].class);
            List<PostDto> postList = Arrays.asList(posts);
            Collections.reverse(postList);
            return postList;
        } catch(RestClientException e) {
            e.printStackTrace();
            throw new BackendServerError();
        }

    }
}
