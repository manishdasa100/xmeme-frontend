package com.xmeme.frontend.exceptions;

public class BackendServerError extends ApplicationError {
    
    public static final String DEFAULT_MSG = "Backend server could not be accessed!";

    public BackendServerError() {
        super(DEFAULT_MSG);
    }

    public BackendServerError(String msg) {
        super(msg);
    }
}
